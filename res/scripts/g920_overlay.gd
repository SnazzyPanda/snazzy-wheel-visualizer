extends Node

# TODO: support other buttons (such as b) for things like handbrake...

#export var refreshrate = 0.2
export var refreshrate = 1/30
var timeSinceLastUpdate = 0

onready var accelNode = find_node('accel')
onready var brakeNode = find_node('brake')
onready var clutchNode = find_node('clutch')
onready var wheelNode = find_node('wheel')

onready var shiftupNode = find_node('ShiftUp')
onready var shiftdownNode = find_node('ShiftDown')

var wheelMaxDegRotation = 450

var brakerecinput = false
var gasrecinput = false
var clutchrecinput = false


# Called when the node enters the scene tree for the first time.
func _ready():
	accelNode.value = 0
	brakeNode.value = 0
	clutchNode.value = 0
#

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# run no faster than defined refreshrate
	timeSinceLastUpdate += delta
	if(timeSinceLastUpdate < refreshrate):
		return
	#
	
	timeSinceLastUpdate = 0
	
	# NOTE: Steering stay at 0 until there is input... (regardless of actual position)
	var steeringVal = Input.get_joy_axis(0,0)
	
	# wheel turned ranges -1 to 1 (0 neutral)
	
	# values range from -1 to 1 (-1 is fully depressed, 1 is neutral state). normalize to -2 to 0
	var gasVal = Input.get_joy_axis(0,1) - 1
	var brakeVal = Input.get_joy_axis(0,2) - 1
	var clutchVal = Input.get_joy_axis(0,3) - 1
	
	# wait for input before we start doing anything. The default value is 0 before input, but 1 after input
	if(!gasrecinput && gasVal != -1):
		gasrecinput = true
	if(!brakerecinput && brakeVal != -1):
		brakerecinput = true
	if(!clutchrecinput && clutchVal != -1):
		clutchrecinput = true
	
	if(gasrecinput):
		accelNode.value = int(gasVal * -50)
	if(brakerecinput):
		brakeNode.value = int(brakeVal * -50)
	if(clutchrecinput):
		clutchNode.value = int(clutchVal * -50)
	#
	
	wheelNode.set_rotation_degrees(steeringVal * wheelMaxDegRotation)
	
	if(Input.is_joy_button_pressed(0, JOY_BUTTON_4)) :
		shiftupNode.visible = true
	else:
		shiftupNode.visible = false
	if(Input.is_joy_button_pressed(0, JOY_BUTTON_5)) :
		shiftdownNode.visible = true
	else:
		shiftdownNode.visible = false
	#
#
