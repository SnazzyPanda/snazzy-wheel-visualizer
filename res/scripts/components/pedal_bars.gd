extends Node

export var refreshrate = float(1) / 30
# duration in seconds

var timeSinceLastRefresh = 0

onready var accelNode = find_node('accelBar')
onready var brakeNode = find_node('brakeBar')
onready var clutchNode = find_node('clutchBar')

onready var accelNumLabel = accelNode.get_node('accelNum')
onready var brakeNumLabel = brakeNode.get_node('brakeNum')
onready var clutchNumLabel = clutchNode.get_node('clutchNum')


var brakerecinput = false
var gasrecinput = false
var clutchrecinput = false

func _ready():
	accelNode.value = 0
	brakeNode.value = 0
	clutchNode.value = 0
	
	accelNumLabel.add_color_override("font_color", Color(0,0,0,1))
	brakeNumLabel.add_color_override("font_color", Color(0,0,0,1))
	clutchNumLabel.add_color_override("font_color", Color(0,0,0,1))
#

func _process(delta):
	# run no faster than defined refreshrate
	timeSinceLastRefresh += delta
	
	if(timeSinceLastRefresh >= refreshrate):
		timeSinceLastRefresh = 0
		updatePedalBars()
	#
#


func updatePedalBars():
	# values range from -1 to 1 (-1 is fully depressed, 1 is neutral state). normalize to -2 to 0
	var gasVal = Input.get_joy_axis(0,1) - 1
	var brakeVal = Input.get_joy_axis(0,2) - 1
	var clutchVal = Input.get_joy_axis(0,3) - 1
	
	# wait for input before we start doing anything. The default value is 0 before input, but 1 after input
	if(!gasrecinput && gasVal != -1):
		gasrecinput = true
	if(!brakerecinput && brakeVal != -1):
		brakerecinput = true
	if(!clutchrecinput && clutchVal != -1):
		clutchrecinput = true
	
	if(gasrecinput):
		accelNode.value = int(gasVal * -50)
		if(accelNumLabel != null):
			accelNumLabel.text = str(int(accelNode.value))
	if(brakerecinput):
		brakeNode.value = int(brakeVal * -50)
		if(brakeNumLabel != null):
			brakeNumLabel.text = str(int(brakeNode.value))
	if(clutchrecinput):
		clutchNode.value = int(clutchVal * -50)
		if(clutchNumLabel != null):
			clutchNumLabel.text = str(int(clutchNode.value))
	#
#

