extends Node


export var refreshrate = float(1) / 30
# duration in seconds

var timeSinceLastRefresh = 0

onready var shiftupNode = find_node('ShiftUp')
onready var shiftdownNode = find_node('ShiftDown')

func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# run no faster than defined refreshrate
	timeSinceLastRefresh += delta
	
	if(timeSinceLastRefresh >= refreshrate):
		timeSinceLastRefresh = 0
		updateShiftDisplay()
	#
#


func updateShiftDisplay():
	if(Input.is_joy_button_pressed(0, JOY_BUTTON_4)) :
		shiftupNode.visible = true
	else:
		shiftupNode.visible = false
	if(Input.is_joy_button_pressed(0, JOY_BUTTON_5)) :
		shiftdownNode.visible = true
	else:
		shiftdownNode.visible = false
	#
#

