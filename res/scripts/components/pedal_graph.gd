extends Node


# NOTE: graph is sensitive to how many points it needs to process. Watch the duration and refresh rate numbers
export var graphDuration = 10
export var graphRefreshRate = float(1) / 30
const graphWidth = 100
var numGraphPoints = graphDuration / graphRefreshRate

var timeSinceLastRefresh = 0
var timeSinceLastGraph = 0

onready var Graph = get_tree().root.get_child(0)
onready var GraphBG = find_node('GraphBG')
onready var brakegline = find_node('BrakeLine')
onready var gasgline = find_node('GasLine')

onready var upshiftReference = find_node('UpShiftLine')
onready var downshiftReference = find_node('DownShiftLine')
onready var ShiftsContainer = find_node('ShiftsContainer')

onready var upshiftWidth = upshiftReference.points[2].x
onready var upshiftHeight = upshiftReference.points[2].y
onready var downshiftWidth = downshiftReference.points[2].x
onready var downshiftHeight = downshiftReference.points[1].y

export var ShiftVerticalOffset = 10

onready var upshiftVerticalOffset = 0 #upshiftHeight + ShiftVerticalOffset
onready var downshiftVerticalOffset = 0 #downshiftHeight + ShiftVerticalOffset

var brakerecinput = false
var gasrecinput = false
var clutchrecinput = false

var trackedUpshifts = []
var trackedDownshifts = []
var trackedShifts = []

onready var startX = GraphBG.rect_size.x #GraphBG.rect_global_position.x - GraphBG.rect_size.x
onready var endX = GraphBG.rect_position.x

var graphYOffset = 0

var shiftXSteps = 1

func _ready():
	graphYOffset = GraphBG.rect_position.y
	initGraph()
	shiftXSteps = (startX - endX) / numGraphPoints
#


func _process(delta):
	# run no faster than defined refreshrate
	timeSinceLastGraph += delta

	
	if(timeSinceLastGraph >= graphRefreshRate):
		timeSinceLastGraph = 0
		var gasVal = Input.get_joy_axis(0,1) - 1
		var brakeVal = Input.get_joy_axis(0,2) - 1
		var clutchVal = Input.get_joy_axis(0,3) - 1
		
		
		var shiftUp = false
		var shiftDown = false
		
		if(Input.is_joy_button_pressed(0, JOY_BUTTON_4)) :
			shiftUp = true
		if(Input.is_joy_button_pressed(0, JOY_BUTTON_5)) :
			shiftDown = true

		
		# wait for input before we start doing anything. The default value is 0 before input, but 1 after input
		if(!gasrecinput && gasVal != -1):
			gasrecinput = true
		if(!brakerecinput && brakeVal != -1):
			brakerecinput = true
		if(!clutchrecinput && clutchVal != -1):
			clutchrecinput = true
		
		if(!gasrecinput):
			gasVal = 0
		if(!brakerecinput):
			brakeVal = 0
		if(!clutchrecinput):
			clutchVal = 0
		#

		updatePedalGraph(int(gasVal * -50), int(brakeVal * -50), int(clutchVal * -50), shiftUp, shiftDown)
	#
#


func initGraph():
	var graphXStep = graphWidth / numGraphPoints
	
	var curX = 0
	for i in (numGraphPoints):
		brakegline.add_point(Vector2(i * graphXStep, graphYOffset))
		gasgline.add_point(Vector2(i * graphXStep, graphYOffset))
	#
#

func updatePedalGraph(gas, brake, clutch, shiftUp = false, shiftDown = false):
	for i in (numGraphPoints + 1):
		var curStep = brakegline.get_point_position(i)
		
		if(i == numGraphPoints):
			if(gas < 1):
				gas = 0
			#
			if(brake < 1):
				brake = 0
			#
			
			if(shiftUp):
				var newshift = upshiftReference.duplicate()
				newshift.visible = true
				newshift.position = Vector2(startX - upshiftWidth, (Graph.rect_position.y - upshiftVerticalOffset))
				trackedShifts.append(newshift)
				ShiftsContainer.add_child(newshift)
			if(shiftDown):
				var newshift = downshiftReference.duplicate()
				newshift.visible = true
				newshift.position = Vector2(startX - downshiftWidth, (Graph.rect_position.y - downshiftVerticalOffset))
				trackedShifts.append(newshift)
				ShiftsContainer.add_child(newshift)
			#
			
			brakegline.set_point_position(i, Vector2(curStep.x, -brake + graphYOffset))
			gasgline.set_point_position(i, Vector2(curStep.x, -gas + graphYOffset))
		else:
			brakegline.set_point_position(i, Vector2(curStep.x, brakegline.get_point_position(i + 1).y))
			gasgline.set_point_position(i, Vector2(curStep.x, gasgline.get_point_position(i + 1).y))
		#
	#
	
	# move the shift nodes and mark any that should be removed
	var removeIndex = []
	for i in range(trackedShifts.size()):
		trackedShifts[i].position.x = trackedShifts[i].position.x - shiftXSteps
		if(trackedShifts[i].position.x < -upshiftWidth):
			removeIndex.append(i)
		#
	#
	
	# remove the node from the tree and remove it from the tracking list
	if(len(removeIndex) > 0):
		removeIndex.sort()
		removeIndex.invert()
		for i in removeIndex:
			ShiftsContainer.remove_child(trackedShifts[i])
			trackedShifts.remove(i)
#

# try to generate resonable fake numbers
func genFakeGraphVal(inputval, sourceline):
	#return inputval
	var tmp = sourceline.get_point_position(99)
	var tmp2 = sourceline.get_point_position(98)
	
	inputval = -tmp.y
	
	if(inputval <= 0 || inputval >= 100):
		if(inputval <= 0):
			inputval = 1
		else:
			inputval = 99
	else:
		var older = -tmp2.y
		var newer = -tmp.y
		if(older > newer):
			inputval = newer - (older - newer)
			
			if(inputval <= 0):
				inputval = 0
		else:
			inputval = newer + (newer - older)
			if(inputval >= 100):
				inputval = 100
		#
	#
	return inputval
#

