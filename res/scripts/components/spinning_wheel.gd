extends Node

export var refreshrate = float(1) / 30
export var wheelMaxDegRotation = 270
onready var wheelNode = find_node('wheel')

var timeSinceLastRefresh = 0

func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# run no faster than defined refreshrate
	timeSinceLastRefresh += delta
	
	if(timeSinceLastRefresh >= refreshrate):
		timeSinceLastRefresh = 0
		updateSteeringwheel()


func updateSteeringwheel():
	# NOTE: Steering stay at 0 until there is input... (regardless of actual position)
	# wheel turned ranges -1 to 1 (0 neutral)
	var steeringVal = Input.get_joy_axis(0,0)
	wheelNode.set_rotation_degrees(steeringVal * wheelMaxDegRotation)
#
