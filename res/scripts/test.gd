extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

#export var refreshrate = 0.2
export var refreshrate = 1/30
var timeSinceLastUpdate = 0

onready var accelNode = find_node('accel')
onready var brakeNode = find_node('brake')
onready var clutchNode = find_node('clutch')
onready var wheelNode = find_node('wheel')

onready var shiftupNode = find_node('ShiftUp')
onready var shiftdownNode = find_node('ShiftDown')

var wheelMaxDegRotation = 450

var brakerecinput = false
var gasrecinput = false
var clutchrecinput = false


# Called when the node enters the scene tree for the first time.
func _ready():
	#print(str(Input.get_joy_name(0)))
	#print(str(Input.get_joy_name(1)))
	
	#print(str(Input.get_joy_axis_string(JOY_AXIS_0)))
	#print(str(Input.get_joy_axis_string(JOY_AXIS_1)))
	#print(str(Input.get_joy_axis_string(JOY_AXIS_2)))
	#print(str(Input.get_joy_axis_string(JOY_AXIS_3)))
	#print(str(Input.get_joy_axis_string(JOY_AXIS_4)))
	#print(str(Input.get_joy_axis_string(JOY_AXIS_5)))
	#print(str(Input.get_joy_axis_string(JOY_AXIS_6)))
	#print(str(Input.get_joy_axis_string(JOY_AXIS_7)))
	
	#accelNode = find_node('accel')
	accelNode.value = 0
	brakeNode.value = 0
	clutchNode.value = 0
	
	#print(str(accelNode))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# run no faster than defined refreshrate
	timeSinceLastUpdate += delta
	if(timeSinceLastUpdate < refreshrate):
		return
	
	
	#var val = int(floor(rand_range(0, 100)))
	#accelNode.value = val
	#print(str(val))
	print('')
	
	timeSinceLastUpdate = 0
	
	# NOTE: Steering stay at 0 until there is input... (regardless of actual position)
	var steeringVal = Input.get_joy_axis(0,0)
	
	var gasVal = Input.get_joy_axis(0,1) - 1
	var brakeVal = Input.get_joy_axis(0,2) - 1
	var clutchVal = Input.get_joy_axis(0,3) - 1
	
	# wheel turned ranges -1 to 1 (0 neutral)
	#print('Steering: ' + str(Input.get_joy_axis(0,0)))
	#print('Gas: ' + str(gasVal))
	#print('Brake: ' + str(brakeVal))
	#print('Clutch: ' + str(clutchVal))
	
	# wait for input before we start doing anything. The default value is 0 before input, but 1 after input
	if(!gasrecinput && gasVal != -1):
		gasrecinput = true
	if(!brakerecinput && brakeVal != -1):
		brakerecinput = true
	if(!clutchrecinput && clutchVal != -1):
		clutchrecinput = true
	
	if(gasrecinput):
		accelNode.value = int(gasVal * -50)
	if(brakerecinput):
		brakeNode.value = int(brakeVal * -50)
	if(clutchrecinput):
		clutchNode.value = int(clutchVal * -50)

	#accelNode.value = int(gasVal * -50)
	#brakeNode.value = int(brakeVal * -50)
	#clutchNode.value = int(clutchVal * -50)
	
	
	wheelNode.set_rotation_degrees(steeringVal * wheelMaxDegRotation)
	
	
	#testing...
	#wheelNode.set_rotation_degrees(rand_range(-1, 1) * wheelMaxDegRotation)
	#accelNode.value = int(floor(rand_range(0, 100)))
	#brakeNode.value = int(floor(rand_range(0, 100)))
	#clutchNode.value = int(floor(rand_range(0, 100)))
	
	
	
	
	if(Input.is_joy_button_pressed(0, JOY_BUTTON_4)) :
		shiftupNode.visible = true
	else:
		shiftupNode.visible = false
	if(Input.is_joy_button_pressed(0, JOY_BUTTON_5)) :
		shiftdownNode.visible = true
	else:
		shiftdownNode.visible = false
	pass
