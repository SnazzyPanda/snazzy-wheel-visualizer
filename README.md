# Snazzy Wheel Visualizer

A racing wheel visualizer haphazardly thrown together in Godot.

So far, only tested/run with a Logitech G920 Driving Force wheel.
